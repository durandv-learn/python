import math
import time

debug = True


def mostrar(texto):
    """
    Imprime un texto
    :param texto: texto a imprimir
    :return: print
    """
    if debug:
        print(texto)
    else:
        pass


def imprimir_numeros(ndesde, nhasta):
    """
    Imprime numeros empezando por ndesde y terminando en nhasta
    :param ndesde: nro desde
    :param nhasta: nro hasta
    :return: print
    """
    for i in range(ndesde, nhasta + 1):
        mostrar(i)


def calcular_interes(capital, tasainteres, cantidadanos):
    """
    Calcula el interes total obtenido
    :param capital: cantidad de pesos
    :param tasainteres: tasa de interés
    :param cantidadanos: cantidad de años
    :return: interes
    """
    interes = (capital * math.pow((1 + (tasainteres / 100)), cantidadanos))
    mostrar(interes)
    return interes


def convertir_fahrenheit_celsius(gradosf):
    """
    Convierte de Fahrenheit a Celsius
    :param gradosf: grados F
    :return: grados Celsius
    """
    valor = gradosf - 32 / (9 / 5)
    mostrar(valor)
    return valor


def repetir_str(cadena, n):
    """
    Devuelve un str repetido n veces
    :param cadena: string de entrada
    :param n: nro de veces
    :return: string
    """
    valor = cadena * n
    mostrar(valor)
    return valor


def calcular_segundos(h, m, s):
    """
    Devuelve la cantidad de segundos
    :param h: horas
    :param m: minutos
    :param s: segundos
    :return: int
    """
    valor = (h * 60 * 60 + m * 60 + s)
    mostrar(valor)
    return valor


def es_bisiesto(y):
    """
    Devuelve si un año es bisiesto
    :param y: año
    :return: True / False
    """
    valor = ((y % 400 == 0) or (y % 4 == 0 and y % 100 != 0))
    mostrar(valor)
    return valor


def promedio_notas():
    """
    Ejercicio 5.7.1. Escribir un programa que permita al usuario ingresar un conjunto de notas, preguntando a cada
    paso si desea ingresar más notas, e imprimiendo el promedio correspondiente.
    :return: int
    """
    suma = 0
    cant = 0
    while True:
        nota = input("Ingrese una nota (* finaliza): ")
        if nota == "*":
            print("Promedio: {} ".format((suma / cant)))
            break
        else:
            cant += 1
            suma += int(nota)


def descomponer_numero_en_primos(k):
    """
    Ejercicio 5.7.2. Escribir una función que reciba un número entero k e imprima su descomposición en factores primos.
    :param k: numero a descomponer
    :return: str
    """
    primos = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59]
    while k > 1:
        i = 0
        while True:
            if k % primos[i] == 0:
                print(primos[i])
                k = k / primos[i]
                break
            if primos[i] == primos[-1]:
                print("?")
                break
            i += 1


def ingresar_password():
    """
    Ejercicio 5.7.3. Manejo de contraseñas
        a) Escribir un programa que contenga una contraseña inventada, que le pregunte al usua-
        rio la contraseña, y no le permita continuar hasta que la haya ingresado correctamente.
        b) Modificar el programa anterior para que solamente permita una cantidad fija de inten-
        tos.
        c) Modificar el programa anterior para que después de cada intento agregue una pausa
        cada vez mayor, utilizando la función sleep del módulo time .
        d) Modificar el programa anterior para que sea una función que devuelva si el usuario
        ingresó o no la contraseña correctamente, mediante un valor booleano ( True o False ).
    :return:
    """
    intentos = 3
    password = "123"
    for i in range(0, intentos):
        time.sleep(i)
        ingreso = input("Ingrese un password: ")
        if password != ingreso:
            print("Password incorrecto.. intente nuevamente...")
        else:
            print("Password correcto!")
            return True
        if i == intentos - 1:
            print("Intentos maximos alcanzados")
            return False


def tupla_ordenada(t):
    """
    Ejercicio 7.6.1. Escribir una función que reciba una tupla de elementos e indique si se encuen-
    tran ordenados de menor a mayor o no.
    :param t: tupla de elementos
    :return: True/False
    """
    for i in range(len(t) - 1):
        if i < len(t):
            if t[i] > t[i + 1]:
                return False
    return True


def tuplas_a_diccionario(l):
    """
    Ejercicio 9.5.1. Escribir una función que reciba una lista de tuplas, y que devuelva un diccio-
    nario en donde las claves sean los primeros elementos de las tuplas, y los valores una lista con
    los segundos.
    :param l: lista
    :return: dict
    """
    diccionario = {}
    for e in l:
        if e[0] not in diccionario:
            diccionario[e[0]] = [e[1]]
        else:
            diccionario[e[0]].append(e[1])
    return diccionario


def contar_palabras(texto):
    """
    Ejercicio 9.5.2. Diccionarios usados para contar.
    a) Escribir una función que reciba una cadena y devuelva un diccionario con la cantidad
    de apariciones de cada palabra en la cadena
    :param texto:
    :return:
    """
    diccionario = {}
    lista = texto.lower().split()
    for i in lista:
        if i not in diccionario:
            diccionario[i] = 1
        else:
            diccionario[i] += 1
    return diccionario


# imprimir_numeros(10, 20)
# calcular_interes(100000, 43, 2)
# for i in range(0, 120, 10):
#     convertir_fahrenheit_celsius(i)

# repetir_str("hola", 3)

# calcular_segundos(1, 0, 0)

# es_bisiesto(2020)

# promedio_notas()

# descomponer_numero_en_primos(117)

# ingresar_password()

# print(tupla_ordenada((1, 2, 3, 5)))

# l = [('Hola', 'don Pepito'), ('Hola', 'don Jose'), ('Buenos', 'días')]
# print(tuplas_a_diccionario(l))

print(contar_palabras("Que calor que hace hoy"))
