class Intervalo:
    """
    Clase Intervalo
    """

    def validar_parametro(self, p):
        if not isinstance(p, (int)):
            raise TypeError("{} no es un valor numérico".format(p))
        return p

    def __init__(self, desde, hasta):
        """
        Consturctor
        :param desde:
        :param hasta:
        """
        self.desde = self.validar_parametro(desde)
        self.hasta = self.validar_parametro(hasta)
        if self.desde > self.hasta:
            raise ValueError("Parametro desde mayor a hasta")

    def __str__(self):
        return "Intervalo({},{})".format(self.desde, self.hasta)

    def __repr__(self):
        pass

    def duracion(self):
        return self.hasta - self.desde

    def interseccion(self, inter):
        inicio, fin = -1, -1

        if self.desde >= inter.desde and self.desde <= inter.hasta:
            inicio = self.desde
        elif inter.desde >= self.desde and inter.desde <= self.hasta:
            inicio = inter.desde

        if self.hasta >= inter.desde and self.hasta <= inter.hasta:
            fin = self.hasta
        elif inter.hasta >= self.desde and inter.hasta <= self.hasta:
            fin = inter.hasta

        if inicio < 0 or fin < 0:
            raise ValueError('No existe interseccion entre los Intervalos')

        return Intervalo(inicio, fin)

    def union(self, inter):
        # Tira excepción si no hay intersección
        i = self.interseccion(inter)

        # Si hay intersección calculamos la unión
        inicio = self.desde if self.desde <= inter.desde else inter.desde;
        fin = self.hasta if self.hasta >= inter.hasta else inter.hasta;

        return Intervalo(inicio, fin)


inter1 = Intervalo(100, 200)
inter2 = Intervalo(110, 300)
print("Intersección entre {} y {} = {}".format(inter1, inter2, inter1.interseccion(inter2)))
print("Unión entre {} y {} = {}".format(inter1, inter2, inter1.union(inter2)))
